# FarmBot Navigation
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Project Technologies:
- Operating System: Ubuntu 20
- Framework: ROS Galactic
- Simulator: Gazebo (version 9+)

## Dependencies
- **Ubuntu 20**
- **ROS Galactic** 

## Building and Running the package
**Note-** Be sure ros is sourced before

1. Create your workspace
    ```bash
    mkdir -p <your_ws_name>/src
    ```
2. Clone the repo
    ```bash
    cd <your_ws>/src
    git clone https://gitlab.com/pratik_bhujbal/task2.git
    ```

3. Install ROS dependencies:
    ```bash
    cd ~/<your_ws>
    rosdep install --from-paths src --ignore-src -r -y
    sudo apt install ros-glactic-navigation2 ros-glactic-nav2-bringup
    sudo apt-get install ros-galactic-teleop-twist-keyboard
    sudo apt install ros-galactic-xacro
    sudo apt install ros-galactic-gazebo-ros-pkgs
    sudo apt install ros-galactic-robot-localization 
    ```
4. Building and running the package:
    ```bash
    cd ~/<your_ws>
    colcon build
    source install/setup.bash
    ros2 launch farmbot navigation.launch.py
    ```
5. Teleoperation (In another terminal):  
    ```bash
    ros2 run teleop_twist_keyboard teleop_twist_keyboard
    ```
6. Navigation in environment :
- Click “Nav2 Goal” button in RViz, and click on your goal in RViz.  
**or**
- Run folllowing command (In another terminal)
    ```bash
    ros2 topic pub /goal_pose geometry_msgs/PoseStamped "{header: {stamp: {sec: 0}, frame_id: 'map'}, pose: {position: {x: -1.0, y: 0.0, z: 0.0}, orientation: {w: 1.0}}}"
    ```
## Result
<p align="center">
<img src="results/nav.gif" />
</p>
